"""
CREO STTLS Hirth files tool

This tool is created to update Hirth files with Settels Parameters and Relations. This is a "quick-and-dirty" toool. The connection is made with CREOSON.

created by: Dosco Vijayakumar
created on: 22-07-2021

Bitbucket repository: /settelsplmservices/creo_hirth_files_tool.git
"""

import creopyson
from subprocess import Popen
from tkinter import Tk
from tkinter import messagebox
from sys import exit

# connect to client
c = creopyson.Client()
try:
    c.connect()
except:
    Popen("C:\ptc\CreosonServer\creoson_run.bat", cwd=r"C:\ptc\CreosonServer")
    c.connect()

c.is_creo_running() # Return a boolean.

# if CREO is not running
if not c.is_creo_running():
    print('no CREO session found')
    exit()

# get active model
attempts = 0

while attempts < 2:
    try:
        curmodel = c.file_get_active()["file"]
        break
    except:
        attempts+=1
        print('Getting file failed, trying again.. attempts left: ' + str(2-attempts))
# print(curmodel)

# check model type
prt_file = False
asm_file = False
if curmodel.endswith('prt'):
    if curmodel.startswith('n-'):
        print('You are not allowed to update N-items')
        root = Tk()
        root.withdraw()
        messagebox.showerror('Update failed', 'You are not allowed to update N-items')
        exit()
    else:
        print("Updating " + curmodel + "..")
        prt_file = True
elif curmodel.endswith('asm'):
    print("Updating " + curmodel + "..")
    asm_file = True
# elif curmodel.endswith('drw'):
#     root = Tk()
#     root.withdraw()
#     messagebox.showerror('Update failed', 'A template update can only be done on a *.prt or *.asm file')
#     exit()
else:
    root = Tk()
    root.withdraw()
    messagebox.showerror('Update failed', 'A template update can only be done on a *.prt or *.asm file')
    exit()

# update restricted definitions
print("updating restriction definitions..")
c.interface_mapkey('~ Command `ProCmdMmParams` ;\
~ Activate `relation_dlg` `PBApplyDefs` ;\
~ Activate `restr_cnf` `ExtraDefCB` 1 ;\
~ Activate `restr_cnf` `ApplyPB` ;\
~ Activate `UI Message Dialog` `ok` ;\
~ Activate `relation_dlg` `PB_OK` ;')

if not c.parameter_exists('BEARBEITER'):
        c.parameter_set('BEARBEITER','-',curmodel,'STRING')
        print('Added parameter BEARBEITER')
if not c.parameter_exists('BENENNUNG'):
        c.parameter_set('BENENNUNG','-',curmodel,'STRING')
        print('Added parameter BENENNUNG')
if not c.parameter_exists('MASS_MANUAL'):
        c.parameter_set('MASS_MANUAL','0',curmodel,'DOUBLE')
        print('Added parameter MASS_MANUAL')
if not c.parameter_exists('TC_CLEANLINESS'):
        c.parameter_set('TC_CLEANLINESS','N/A',curmodel,'STRING')
        print('Added parameter TC_CLEANLINESS')
if not c.parameter_exists('TC_MEASURING_REPORT'):
        c.parameter_set('TC_MEASURING_REPORT','N/A',curmodel,'STRING')
        print('Added parameter TC_MEASURING_REPORT')

# # update template
# print("updating template..")
# c.interface_mapkey('%uptemplate;')

print("Hirth files update finished")
print('Use the standard "update CREO template" button to finish the update')